# Usage of benchmark

## Overview

This directory contains the performance benchmark of FlexRaft. We build a distributed key-value service atop FlexRaft using RocksDB as the storage engine. The client issues Put/Get requests to the cluster and records the performance statistics. 

There are three benchmark tests and each benchmark consists of the code for the server side and client side respectively. 

* Write benchmark: testing the write performance of the distributed key-value store.   ``bench_server.cc/bench_client.cc`` . 
* Read benchmark:  testing the read performance of the distributed key-value store. ``bench_server.cc/readbench_client.cc``. 
* YCSB benchmark: testing the performance of the distributed key-value store using YCSB benchmarks. ``ycsb_server.cc/ycsb_client.cc``

---

## Configure the cluster and network

### Cluster

Both the key-value server and the Raft server need the cluster information, which is recorded in a single configuration file. In this configuration file, each line represents a node in the following format: 

```
<node id> <node ip:raft port> <node ip:kv port> <log file path> <db path>
```

The ``bench/example_cluster.cfg`` file gives an example of this configuration file. The users of FlexRaft are supposed to fill in their own server information. 

### Network

Using the following commands to limit the network bandwidth to 1Gbps: 

```
 tc qdisc del dev eth0 root
 tc qdisc add dev eth0 root handle 1:  htb default 11
 tc class add dev eth0 parent 1: classid 1:11 htb rate 1Gbit ceil 1Gbit
```

---

## Run the benchmark

### Write benchmark

On the server side, execute the ``bench_server`` binary on each server using the following command:

```
./bench_server <path to configuration file> <node id>
```

Note that the node id and the IP address of the server should match the configuration file. 

On the client side, execute the ``bench_client`` binary using the following command:

```
./bench_client <path to configuration file> <client_id> <value size> <write count>
```

For example, starting up a client that sends 10000 key-value requests with each carrying a value of 1MiB requires the following commands:

```
./bench_client example_cluster.cfg 0 1M 10000
```

The ``client_id`` parameter doesn't matter in this test and can always be 0. 

### Read benchmark

Executing the following commands on the server side and client side to test read performance:

```
./bench_server <path to configuration file> <node id>
./bench_client <path to configuration file> <client_id> <value size> <write count>
./readbench_client <path to configuration file> <client_id> <value side> <read count> <repeated read>
```

Note that the read benchmark requires **executing the write benchmark** as a first step to ingest data into the key-value store. The ``readbench_client`` uses a parameter called ``repeated_read`` to control the number of read touching each record, e.g., setting this parameter to 10 means that each key is read 10 times. 

### YCSB benchmark

Executing the following commands on the server side and client side respectively: 

```
./ycsb_server <path to configuration file> <node id>
./ycsb_client <path to configuration file> <client number> <value size> <op count> <ycsb_type>
```

* ``<ycsb_type>:`` YCSB_A, YCSB_B, YCSB_C, YCSB_D, YCSB_F. 
* ``<client number>:   `` YCSB benchmark supports multiple clients running within a single process. Each client spawns a thread to execute the YCSB benchmark and reports their throughput individually. We calculate the overall throughput by summing up the throughput of each thread.  

 