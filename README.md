# Overview

FlexRaft is a variant of Raft protocol that exploits an adaptive erasure coding scheme to minimize network and storage costs. This repository contains our implementation of an academic prototype of FlexRaft. 

# Codebase

The FlexRaft prototype codebase consists of the following modules:

* ``RCF``: The third-party RPC framework we use. See this [link](https://www.deltavsoft.com/) for more details. 
* ``raft``: Implementation of FlexRaft protocol. 
* ``kv``: A distributed key-value service built atop FlexRaft. RocksDB is used to be the state machine. 
* ``bench``: Code for evaluating the performance of our system. 
* ``third-party``: Related third-party modules. 

The structure of FlexRaft protocol and the corresponding implementation of individual modules are as follows:

![avatar](figs/flexraft.pdf)

---

# Build

## Build tools

FlexRaft requires some necessary tool-chains for building the project. In CentOS, we use the following commands to install them: 

```
yum -y update
yum install -y centos-release-scl-rh
yum install -y devtoolset-10
yum install -y cmake cmake3 make automake gcc gcc-c++ kernel-devel git
yum install -y libtool autoconf
```

For other linux-Distros, install the corresponding counterparts. 

## Third-party libraries

FlexRaft uses [RocksDB](https://github.com/facebook/rocksdb) as a single-node storage engine and googletest for unit tests. Run the following command to download and install them: 

```
python3 scripts/install_dependencies.py
```

FlexRaft exploits [intel isa-l library](https://github.com/intel/isa-l) for erasure coding operations. Execute the following command to install the ISA-L library and nasm assembler (to build the ISA-L library). 

```
wget https://www.nasm.us/pub/nasm/releasebuilds/2.14.02/nasm-2.14.02.tar.gz
tar -zxvf nasm-2.14.02.tar.gz
cd nasm-2.14.02
./autogen.sh
./configure
 make
 make install
```

```
git clone git@github.com:intel/isa-l.git
cd isa-l
./autogen.sh
./configure
make
make install
```

## Build the project

In the root path of the repository, simply type: ``make`` to build the whole project. One may build the project using cmake: 

```
cmake -B build -S . -DCMAKE_BUILD_TYPE=Release
cmake --build build
```

---

# Run the project

## Run tests

The FlexRaft codebase contains a few unit tests to ensure the correctness of our implementation. The binary files of these tests are located in the directories: ``build/raft/test`` and ``build/kv/test`` . These tests generally simulate a Raft cluster in a single machine by running each Raft instance in each individual thread. 

## Run performance benchmark

The FlexRaft codebase contains the related code to evaluate its performance. Codes and an [explanation](bench/README.md) for usage can be found in the ``bench`` directory. 

